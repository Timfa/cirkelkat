console.log("STARTING");
console.log("--------------------------------------------------");


const { REST, Routes } = require('discord.js');
var auth = require('./auth.json');
var fs = require('fs');

console.log("Requires loaded in.");

var bot = {};

const hont = auth.mode.toLowerCase() == "hont";
const bier = auth.mode.toLowerCase() == "bier";

const Kat = hont ? "Hont" : "Kat";
const kat = hont ? "hont" : "kat";
const Katten = hont ? "Honden" : "Katten";
const katten = hont ? "honden" : "katten";
const KATTEN = hont ? "HONDEN" : "KATTEN";
const KAT = hont ? "HONT" : "KAT";
const ck = hont ? "ch" : "ck";

if (hont)
{
    Kat = "Hont";
    kat = "hont";
    Katten = "Honden";
    katten = "honden";
    KATTEN = "HONDEN";
    KAT = "HONT";
    ck = "ch";
}

console.log("initializing...");

var slashcommands = [
];

slashcommands.push(
    {
        name: ck + "ontgrendel",
        description: kat + " ontgrendelen",
        options: [{ type: 3, name: "hoeveelheid", description: "Hoeveel nieuwe katten?", required: false }]
    }
);

slashcommands.push(
    {
        name: ck + "vergrendel",
        description: kat + " vergrendelen",
        options: [{ type: 3, name: "hoeveelheid", description: "Hoeveel katten vergrendelen?", required: false }]
    }
);

slashcommands.push(
    {
        name: ck + "zet",
        description: "Exact aantal ontgrendelde " + katten + " invoeren",
        options: [{ type: 3, name: "hoeveelheid", description: "Hoeveel katten vergrendelen?", required: false }]
    }
);

slashcommands.push(
    {
        name: "opdatum",
        description: "Opdatum tijd!",
    }
);

slashcommands.push(
    {
        name: ck,
        description: kat + " zien"
    }
);

const rest = new REST({ version: '10' }).setToken(auth.token);

(async () =>
{
    try
    {
        console.log('Started refreshing application (/) commands.');

        await rest.put(Routes.applicationCommands(auth.id), { body: slashcommands });

        console.log('Successfully reloaded application (/) commands.');
    } catch (error)
    {
        console.error(error);
    }
})();

const { Client, GatewayIntentBits } = require('discord.js');
const client = new Client({
    intents:
        [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.MessageContent
        ]
});

var initialized = false;

client.on('interactionCreate', async interaction =>
{
    if (!interaction.isChatInputCommand()) return;
    console.log(interaction);
    let arg = (interaction.options && interaction.options.data.length > 0 && interaction.options.data[0].value) ? interaction.options.data[0].value : 1;
    let id = interaction.guildId;

    let userID = interaction.user;

    if (
        userID == "124136556578603009" //Tim
        || userID == "1183495296743899238" //Gled
        || userID == "140459535293743105" //Stoelpoot
        || userID == "134032886574612481" //koektrommel
        || userID == "223085898198286336" //lars
        || userID == "287293894553501696" //Frank
        || userID == "249985268075986955" //paard
        || userID == "189716214795337729" // Haan
        || interaction.commandName == ck //mag iedereen
    )
    {
        bot.data = bot.data || {};
        bot.data[id] = bot.data[id];
        bot.data[id].katten = bot.data[id].katten;

        switch (interaction.commandName)
        {
            case ck + "ontgrendel":
                bot.data[id].katten -= -arg;
                interaction.reply({ content: ":unlock:\n" + arg + " NIEUWE " + (arg == 1 ? KAT : KATTEN) + " ONTGRENDELD!\n" + bot.data[id].katten + "/5000" });
                break;

            case ck + "vergrendel":
                bot.data[id].katten -= arg;
                interaction.reply({ content: ":lock:\n" + arg + " " + (arg == 1 ? KAT : KATTEN) + " VERGRENDELD!\n" + bot.data[id].katten + "/5000" });
                break;

            case ck + "zet":
                bot.data[id].katten = arg;
                interaction.reply({ content: ":unlock:\n" + arg + " " + (arg == 1 ? KAT : KATTEN) + " ONTGRENDELD!\n" + bot.data[id].katten + "/5000" });
                break;

            case "opdatum":
                interaction.reply({ content: "Nou goed dan, ik ben zo terug." });
                setTimeout(() =>
                {
                    sterf();
                }, 1000);
                break;

            case ck:
                interaction.reply({ content: ":unlock:\nONTGRENDELDE " + KATTEN + ":\n" + bot.data[id].katten + "/5000" });
                break;
        }

        bot.saveData();
    }
    else
    {
        interaction.reply({ content: "Jij mag niet!", ephemeral: true });
    }
});

client.login(auth.token);

bot.data = null;
bot.lastSave = Date.now();

bot.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function (err)
    {
        if (err)
        {
            console.log(err);
            bot.sendMessage({
                to: bot.data.lastServer,
                message: "Failed to create a back-up.",
                typing: false
            });
        }
        else
        {
            if (!silent)
            {
                bot.sendMessage({
                    to: bot.data.lastServer,
                    message: "Bot data back-up created.",
                    typing: false
                });
            }
        }
    });
};

bot.saveData = function (callback, context)
{
    console.log("Saving...");
    try
    {
        if (bot.data != null && bot.data != {} && initialized)
        {
            bot.data = bot.data || {};

            fs.writeFile("./botData.json", JSON.stringify(bot.data), function (err)
            {
                if (err)
                {
                    console.log(err);
                }

                context = context || this;
                callback && callback.call(context, true);
            });
        }
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
};

console.log("savedata function defined");

client.on('ready', () =>
{
    console.log('Connected!');
    console.log('Logged in as: ');
    console.log(bot.username + ' - (' + bot.id + ')');

    fs.readFile('./botData.json', function read(err, data) 
    {
        if (err) 
        {
            data = "{}";
        }
        try
        {
            bot.data = JSON.parse(data);
        }
        catch (e)
        {
            if (bot.data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (bot.data == null || bot.data == {})
        {
            fs.readFile('./botDataBackup.json', function read(err, data)
            {
                if (err)
                {
                    data = "{}";
                }
                try
                {
                    bot.data = JSON.parse(data);
                    console.log("Backup restored");

                    if (bot.data.lastServer)
                    {
                        bot.sendMessage({
                            to: bot.data.lastServer,
                            message: "A crash occurred, but I managed to restore a backup of my memory.",
                            typing: false
                        });
                        saveBackup = false;
                    }
                }
                catch (e)
                {
                    if (bot.data == null)
                    {
                        bot.data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;

                        if (bot.data.lastServer)
                        {
                            bot.sendMessage({
                                to: bot.data.lastServer,
                                message: "A crash occurred, and my memory file was lost :(",
                                typing: false
                            });
                        }
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            bot.saveBackup(true);

            initialized = true;
        }

        bot.data = bot.data || {};

        bot.data.servers = bot.data.servers || [];

        for (var i = 0; i < bot.data.servers.length; i++)
        {
            bot.data[bot.data.servers[i]].adventurelock = [];
        }

        console.log("Checking if I need to send update message...");

        if (bot.data.update && bot.data.update != "no")
        {
            bot.sendMessage({
                to: bot.data.update,
                message: "Ge-opdatumt!",
                typing: false
            });

            bot.data.update = "no";
        }
        else
        {
            console.log("no update notifiy to do");
        }
    });
});

console.log("onready defined");
